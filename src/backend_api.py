"""
VERSION: Demo - 1.1.0
LICENSE: WTFPL
COPYRIGHT: Latency.GG - 2022
"""
from collections import defaultdict
from functools import partial
from typing import List, Tuple, Dict, Union, cast, Any
from urllib.parse import urljoin

import requests

metrics_storage = partial(defaultdict, dict)
Beacon = Dict[str, Union[str, int]]
Metrics = Dict[str, Dict[str, Dict[str, Union[List[Beacon], int, str]]]]


def get_metrics(api: str) -> Tuple[Dict[str, Any], Metrics, Metrics]:
    response = requests.get(urljoin(api, "metrics"))
    response.raise_for_status()
    data = response.json()
    return (
        cast(Dict[str, Any], data["source"]),
        cast(Metrics, data["clean_metrics"] or metrics_storage()),
        cast(Metrics, data["stale_metrics"]),
    )
